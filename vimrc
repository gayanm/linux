" Vundle {{{
set nocompatible              " required
filetype off                  " required
filetype plugin on
set nowrap
" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" let Vundle manage Vundle, required
Plugin 'gmarik/Vundle.vim'
Plugin 'scrooloose/nerdtree'
Plugin 'octol/vim-cpp-enhanced-highlight'
Plugin 'mileszs/ack.vim'
" Add all your plugins here (note older versions of Vundle used Bundle instead of Plugin)
" All of your Plugins must be added before the following line
call vundle#end()            " required
"}}}
" Other {{{
:imap jj <Esc>
" colorscheme badwolf
syntax enable
filetype indent on "load filetype-specific indent files
set number
set tabstop=4 "number of visual spaces per TAB
set shiftwidth=4 " when indenting with '>', use 4 spaces width
set expandtab " On pressing tab, insert 4 spaces
set showcmd " show command in bottom bar
set cursorline " hightlight current line
set wildmenu " visual autocomplete for command menu
set showmatch " hightlight matching [{()}]
set incsearch " search as charactors are entered
set hlsearch " highlight matches
set foldenable "enable folding
set foldlevelstart=10 "close all folds
set foldnestmax=10 "maximum level of nested folding
set foldmethod=indent "fold based on indentation
set so=7
set ruler
set wildmenu
nnoremap <space> za
" prevents skipping lines when a line is multiline
nnoremap j gj
nnoremap k gk
let mapleader="," " leader is comma
" bind noh to space
nnoremap <leader><space> :nohlsearch<CR>
nnoremap <leader>u :GundoToggle<CR>
"}}}
" White Space {{{
" Show tab
set list listchars=tab:>.
" Trailing whitespace.
highlight ExtraWhitespace ctermbg=red guibg=red
autocmd BufEnter * match ExtraWhitespace /\s\+$/
if version>= 700
    autocmd ColorScheme * highlight ExtraWhitespace guibg=red
    autocmd InsertEnter * match ExtraWhitespace /\s\+\%#\@<!$/
    autocmd InsertLeave * match ExtraWhiteSpace /\s\+$/
endif
" Delete trailing white space on save, useful for Python and CoffeeScript ;)
 func! DeleteTrailingWS()
   exe "normal mz"
   %s/\s\+$//ge
   exe "normal `z"
endfunc
autocmd BufWrite *.py :call DeleteTrailingWS()
autocmd BufWrite *.coffee :call DeleteTrailingWS()
"vnoremap <silent> gv :call VisualSelection('gv')<CR>
"map <leader>ss :setlocal spell!<cr>
"}}}
"  leader stuff {{{
" Fast saving
nmap <leader>w :w!<cr>
" Nerdtree toggle
map <C-n> :NERDTreeToggle<CR>
"}}}
" modelines {{{
set modelines=1
" }}}
"vim:foldmethod=marker:foldlevel=0:set number
set rtp+=/usr/local/lib/python2.7/dist-packages/powerline/bindings/vim/
set laststatus=2
set t_Co=256
set background=dark
